import React from 'react';

export default class UberBanner extends React.Component {
    render() {
        return (
            <div className="text-center uber-banner hidden-xs">
                <span className="eximchain-logo"><span>EXIM</span>CHAIN</span> <strong>(<span>EX</span>port <span>IM</span>port on Blockchain)</strong> was founded in 2015 at MIT.
                Join us on <a href="https://t.me/eximchain" target="_blank" rel="noreferrer noopener" title="Join our Telegram channel">Telegram</a> to learn more.
            </div>
        );
    }
}
