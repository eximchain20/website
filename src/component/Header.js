import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { graphql, gql } from 'react-apollo';

class Header extends React.Component  {

    //static proptypes and css
    static propTypes = {
      authenticateUser: PropTypes.func.isRequired,
      history: PropTypes.object.isRequired
    };

    container = {
      marginTop: '100px'
    };

    loginButton = {
      padding: '10px',
      width: '150px',
      background: '#00BFFF',
      color: 'white',
      cursor: 'pointer',
      border: 'none'
    };

    //login with Auth0 client returns authzero token which we can then use to authenticate with graphcool
    _authenticate = () => {
      this.props.webAuth.authorize();

    };

    _isLoggedIn = () => {
      const isLoggedIn = (localStorage.getItem('graphCoolAccessToken') )?true:false;
      return isLoggedIn;
    };
    _logout = () => {
        localStorage.removeItem('graphCoolAccessToken')
        localStorage.removeItem('uid')
        localStorage.removeItem('auth0AccessToken')
        window.location.reload()
      }
    _renderLoggedOut = () => {
      return (
            <button  className="btn btn-white btn-empty ml-lg-3" onClick={this._authenticate}>KYC Login</button>
      )
    };
    _renderLoggedIn =() => {
      return (
            <button  className="btn btn-white btn-empty ml-lg-3" onClick={this._logout}>Log Out</button>
      )
    };
    render() {
        return (
            <header className="bg-dark">
                <div className="container">
                    <nav className="navbar navbar-expand-lg pl-0 pr-0">
                        <a className="navbar-brand" href="/">
                            <h2 className="replica-bold pb-0 mb-0">
                              <span className="eximchain-logo"><span>EXIM</span>CHAIN</span>
                            </h2>
                        </a>

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav11" aria-controls="navbarNav11" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNav11">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <a className="nav-link" href="/#solutions">Solutions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/team">Team</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/documents">Documents</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/road-map">Roadmap</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/faq">FAQ</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/token">Token</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="https://medium.com/eximchain" target="_blank" rel="noopener noreferrer">Blog</a>
                                </li>
                            </ul>
                            {this._isLoggedIn() ? this._renderLoggedIn() : this._renderLoggedOut()}
                        </div>
                    </nav>
                </div>
            </header>
        );
    }
}


const AUTHENTICATE_USER_MUTATION = gql`
  mutation AuthenticateUser($accessToken: String!) {
    authenticateUser(accessToken: $accessToken) {
      id
      token
    }
  }
`;


const AuthenticateUserWithMutation = graphql(AUTHENTICATE_USER_MUTATION, {name:"authenticateUser"})(Header)

export default withRouter(AuthenticateUserWithMutation);
