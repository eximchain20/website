import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="fdb-block footer-small bg-dark" id="footer">
                <div className="fluid-container">
                    <div className="row align-items-center">
                        <div className="col-12 col-md-7 subnav">
                            <ul className="nav justify-content-center justify-content-md-start">
                                <li className="nav-item">
                                    <a className="nav-link" href="/#solutions">Solutions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/team">Team</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/documents">Documents</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/road-map">Roadmap</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/faq">FAQ</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/token">Token</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="https://medium.com/eximchain" target="_blank" rel="noopener noreferrer">Blog</a>
                                </li>
                            </ul>
                        </div>

                        <div className="col-12 col-md-5 mt-4 mt-md-0 text-center text-md-right legal">
                          © 2018 <span className="eximchain-logo"><span>Exim</span>chain</span>, All Rights Reserved. <a href="/privacy" target="_blank" rel="noreferrer noopener">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
