import React, { Component } from 'react';
import { graphql, gql, compose} from 'react-apollo';
//import { InMemoryCache } from 'apollo-cache-inmemory';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import auth0 from 'auth0-js'
import './App.css';
import UberBanner from "./component/UberBanner";
import Header from "./component/Header";
import Footer from "./component/Footer";
import HoveringTelegramButton from "./component/HoveringTelegramButton";


class App extends Component {
  constructor(props) {
    super(props);
    this._webAuth = new auth0.WebAuth({
      audience: 'http://localhost:8080/',
      clientID: 'quxt8iaDfv3lrscQv1mlMKRyJmb9vK26',
      domain: 'eximchain.auth0.com',
      redirectUri: 'https://eximchain.com/loading',
      responseType: 'token',
      scope: 'openid email'
    });


    //use Auth0 token to authenticate with to qraphql microservices\
    // this._authorize = (callback) => {
    //   this._webAuth.parseHash(function(error, authResult) {
    //     if (error){
    //       localStorage.removeItem('graphCoolAccessToken')
    //       localStorage.removeItem('uid')
    //       localStorage.removeItem('auth0AccessToken')
    //
    //       return callback("failed to parse hash", null);
    //
    //     };
    //     if (authResult && authResult.accessToken) {
    //
    //       window.localStorage.setItem('auth0AccessToken', authResult.accessToken)
    //       return callback(null, authResult.accessToken);
    //     }
    //   })
    //
    //
    // }
  }


  static propTypes = {
    history: PropTypes.object.isRequired,
  };

  // _callbackToAuthorizePromise(method, ...args) {
  //    return new Promise(function(resolve, reject) {
  //        return method(...args, function(error, result) {
  //            //if error in storage read reject, result has auth0accessToken to pass into authenticateuser
  //            if (error) return reject({ok:false,err:error});
  //            //authenticate and authorize
  //            const variables = {
  //              accessToken: result,
  //            };
  //            this.props
  //              .authenticateUser({ variables })
  //              .then(res => {
  //                window.localStorage.setItem('graphCoolAccessToken', res.data.authenticateUser.token)
  //                window.localStorage.setItem('uid', res.data.authenticateUser.id)
  //                return resolve({ok:true,err:null});
  //              })
  //              .catch(error => {
  //                return reject({ok:false,err:error});
  //              });
  //        }.bind(this));//bind componenet scope to callback
  //    }.bind(this));//bind component scope to promise
  // }//wrap async call inside a promise to await it
  //
  //
  // async _oAuth(){
  //
  //   //let parse = await this._callbackToParsePromise(this._parseToken);
  //
  //   let authorize = await this._callbackToAuthorizePromise(this._authorize);
  //
  //
  //
  // }//wrap the async calls effect the post condition
  //
  // //first time it loads
  // componentDidMount() {
  //
  //   this._oAuth();
  // }
  // //shouldComponentUpdate(){
  //   //return (this.props.user.user)?true:true
  // //}
  // //when it changes
  // componentDidUpdate() {
  //   this._oAuth();
  // }

  render() {

    return (

      <div style={this.container}>
        <UberBanner/>
        <Header webAuth={this._webAuth} />
            <div className="app-content">{this.props.children}</div>
        <Footer/>
        <HoveringTelegramButton/>

      </div>
    );
  };
}



const AUTHENTICATE_USER_MUTATION = gql`
  mutation AuthenticateUser($accessToken: String!) {
    authenticateUser(accessToken: $accessToken) {
      id
      token
    }
  }
`;


export default (
      graphql(AUTHENTICATE_USER_MUTATION, {
          name: 'authenticateUser'
        })(
          withRouter(App)
        )
);
