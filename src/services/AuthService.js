import decode from 'jwt-decode';
import { browserHistory } from 'react-router';
import auth0 from 'auth0-js';
const ID_TOKEN_KEY = 'auth0AccessToken';


const CLIENT_ID = '{quxt8iaDfv3lrscQv1mlMKRyJmb9vK26}';
const CLIENT_DOMAIN = 'eximchain.auth0.com';
const REDIRECT = 'http://localhost:3000/portal';
const SCOPE = 'openid email profile';
const AUDIENCE = 'http://localhost:8080/';

var auth = new auth0.WebAuth({
  clientID: CLIENT_ID,
  domain: CLIENT_DOMAIN
});

export function login() {
  auth.authorize({
    responseType: 'token',
    redirectUri: REDIRECT,
    audience: AUDIENCE,
    scope: SCOPE
  });
}

export function logout() {
  clearIdToken();
  clearProfile();
  browserHistory.push('/');
}

export function _parseToken(callback){
  if (window.localStorage.getItem('auth0AccessToken')){
   //great already parse and still loged in
   return callback(null,true);
  }else{
   //clear and parseHash
   localStorage.removeItem('graphCoolAccessToken')
   localStorage.removeItem('uid')
   localStorage.removeItem('auth0AccessToken')
   return callback(null, false);
  }
}

//use Auth0 token to authenticate with to qraphql microservices\
export function _authorize(callback){
  if (window.localStorage.getItem('auth0AccessToken')){
    const variables = {
      accessToken: window.localStorage.getItem('auth0AccessToken'),
    };
   //great already parse and still loged in
   return callback(null,variables);
  }else{
   localStorage.removeItem('graphCoolAccessToken')
   localStorage.removeItem('uid')
   localStorage.removeItem('auth0AccessToken')
   return callback(null, false);
  }
}

_callbackToParsePromise(method, ...args) {
   return new Promise(function(resolve, reject) {
       return method(...args, function(error, result) {
           //if error in storage read reject,null result in this case
           if (error) return reject({ok:false,err:error});
           //otherwise try to parse out the auth0AccessToken
           this._webAuth.parseHash(function(error, authResult) {
             if (error) return reject({ok:false,err:error});
             if (authResult && authResult.accessToken) {
               window.localStorage.setItem('auth0AccessToken', authResult.accessToken)
               return resolve({ok:true,err:null});
             }
           }.bind(this));
       }.bind(this));//bind componenet scope to callback
   }.bind(this));//bind component scope to promise
}//wrap async call inside a promise to await it




export function requireAuth(nextState, replace) {
  if (!isLoggedIn()) {
    replace({pathname: '/'});
  }
}

export function getIdToken() {
  return localStorage.getItem(ID_TOKEN_KEY);
}

function clearIdToken() {
  localStorage.removeItem(ID_TOKEN_KEY);
}

function clearProfile() {
  localStorage.removeItem('profile');
  localStorage.removeItem('userId');
}

// Helper function that will allow us to extract the id_token
export function getAndStoreParameters() {
  auth.parseHash(window.location.hash, function(err, authResult) {
    if (err) {
    }

    setIdToken(authResult.idToken);
  });
}

export function getEmail() {
  return getProfile().email;
}

export function getName() {
  return getProfile().nickname;
}

// Get and store id_token in local storage
function setIdToken(idToken) {
  localStorage.setItem(ID_TOKEN_KEY, idToken);
}

export function isLoggedIn() {
  const idToken = getIdToken();
  return !!idToken && !isTokenExpired(idToken);
}

export function getProfile() {
  const token = decode(getIdToken());
  return token;
}

function getTokenExpirationDate(encodedToken) {
  const token = decode(encodedToken);
  if (!token.exp) { return null; }

  const date = new Date(0);
  date.setUTCSeconds(token.exp);

  return date;
}

function isTokenExpired(token) {
  const expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}
