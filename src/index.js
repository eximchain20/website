import React from 'react';
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {ApolloProvider, ApolloClient, createNetworkInterface} from 'react-apollo';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import {IndexPage, SigninPage, SignupPage,DocumentsPage,FAQPage,NewsletterConfirmationPage,PrivacyPage,RoadMapPage,TokenPage,TeamPage,WhiteListPage,WhiteListConfirmationPage,WhiteListEmailPage,FourOFour,FourOO,Profile} from './container/Containers';
import './index.css';
import Loading from './container/Loading.js'



const networkInterface = createNetworkInterface({
  uri: 'https://api.graph.cool/simple/v1/cjd6rufo41ete0118jpqlgktl'
});


// create networkInterface and applyMiddleware to send valid JWT token to authorize ALL ApolloClient graphql queries
networkInterface.use([
  {
    applyMiddleware(req, next) {
      // Create the header object if needed.
      if (!req.options.headers) {req.options.headers = {};}

      // get the authentication token from local storage if it exists
      const token = localStorage.getItem('graphCoolAccessToken');
      if(token){
        req.options.headers.Authorization = token ? `Bearer ${token}` : null;
      }

      next();
    }
  }
]);


//create ApolloClient pass in networkInterface with the Authorization Middleware
const client = new ApolloClient({
  networkInterface
});


//Set up Browser Router so no # redirs and link paths to containers
ReactDOM.render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <App>
        <Switch>
            <Route exact path="/" component={IndexPage}/>
            <Route exact path="/portal" component={Profile} />
            <Route exact path="/loading" component={Loading} />

            <Route exact path="/#portal" component={Profile} />
            <Route exact path="/signup" component={SignupPage}/>
            <Route exact path="/signin" component={SigninPage}/>
            <Route exact path="/documents" component={DocumentsPage}/>
            <Route exact path="/faq" component={FAQPage}/>
            <Route exact path="/newsletter-confirmation" component={NewsletterConfirmationPage}/>
            <Route exact path="/privacy" component={PrivacyPage}/>
            <Route exact path="/road-map" component={RoadMapPage}/>
            <Route exact path="/team" component={TeamPage}/>
            <Route exact path="/token" component={TokenPage}/>
            <Route exact path="/whitelist" component={WhiteListPage}/>
            <Route exact path="/whitelist-confirmation" component={WhiteListConfirmationPage}/>
            <Route exact path="/whitelist-email" component={WhiteListEmailPage}/>
            <Route exact path="/500" component={FourOO}/>
            <Route exact path="*" component={FourOFour}/>
        </Switch>
      </App>
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root')
);
registerServiceWorker();
