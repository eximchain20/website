import React, { Component } from 'react';

export default class PrivacyPage extends Component{

    render(){
        return (
            <section className="fdb-block" id="privacy">
                <div className="container">
                    <h1>Privacy Policy of <a href="http://eximchain.com">eximchain.com</a></h1>
                    <h3>This Application collects some Personal Data from its Users.</h3>

                    <hr />

                    <h2>Data Controller and Owner</h2>
                    <h3>Eximchain,</h3>
                    <h3><a href="mailto:support@eximchain.com">support@eximchain.com</a></h3>

                    <hr />

                    <h2>Types of Data collected</h2>
                    <p>Among the types of Personal Data that this Application collects, by itself or through third parties, there are: Cookies and Usage Data.
                        Other Personal Data collected may be described in other sections of this privacy policy or by dedicated explanation text contextually with the Data collection.
                        The Personal Data may be freely provided by the User, or collected automatically when using this Application.
                        Any use of Cookies - or of other tracking tools - by this Application or by the owners of third party services used by this Application, unless stated otherwise, serves to identify Users and remember their preferences, for the sole purpose of providing the service required by the User.
                        Failure to provide certain Personal Data may make it impossible for this Application to provide its services.
                        Users are responsible for any Personal Data of third parties obtained, published or shared through this Application and confirm that they have the third party's consent to provide the Data to the Owner.</p>

                    <hr />

                    <h2>Mode and place of processing the Data</h2>

                    <h3>Methods of processing</h3>
                    <p>The Data Controller processes the Data of Users in a proper manner and shall take appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data.
                        The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Data Controller, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of the site (administration, sales, marketing, legal, system administration) or external parties (such as third party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Data Controller at any time.</p>

                    <h3>Place</h3>
                    <p>The Data is processed at the Data Controller's operating offices and in any other places where the parties involved with the processing are located. For further information, please contact the Data Controller.
                    </p><h3>Retention time</h3>
                    <p>The Data is kept for the time necessary to provide the service requested by the User, or stated by the purposes outlined in this document, and the User can always request that the Data Controller suspend or remove the data.

                    </p><hr />

                    <h2>The use of the collected Data</h2>
                    <p>The Data concerning the User is collected to allow the Owner to provide its services, as well as for the following purposes:<span className="hide"> Heat mapping and session recording and</span> Analytics.
                        The Personal Data used for each purpose is outlined in the specific sections of this document.</p>

                    <hr />

                    <h2>Detailed information on the processing of Personal Data</h2>
                    <p>Personal Data is collected for the following purposes and using the following services:</p>

                    <div className="row">
                        <div className="col-md-6">
                            <h3>Analytics</h3>
                            <p>The services contained in this section enable the Owner to monitor and analyze web traffic and can be used to keep track of User behavior.
                            </p><h4>Google Analytics (Google Inc.)</h4>
                            <p>Google Analytics is a web analysis service provided by Google Inc. (“Google”). Google utilizes the Data collected to track and examine the use of this Application, to prepare reports on its activities and share them with other Google services.
                                Google may use the Data collected to contextualize and personalize the ads of its own advertising network.</p>
                            <p>Personal Data collected: Cookies and Usage Data.</p>
                            <p>Place of processing: US – <a href="https://www.google.com/intl/en/policies/privacy/" target="_blank">Privacy Policy</a> – <a href="https://tools.google.com/dlpage/gaoptout?hl=en" target="_blank">Opt Out</a></p>
                        </div>
                        <div className="col-md-6 hide">
                            <h3>Heat mapping and session recording</h3>
                            <p>Heat Mapping services are used to display the areas of a page where Users most frequently move the mouse or click. This shows where the points of interest are. These services make it possible to monitor and analyze web traffic and keep track of User behavior.
                                Some of these services may record sessions and make them available for later visual playback.</p>
                            <h4>Hotjar Heat Maps &amp; Recordings (Hotjar Ltd.)</h4>
                            <p>Hotjar is a session recording and heat mapping service provided by Hotjar Ltd.
                                Hotjar honors generic "Do Not Track" headers. This means the browser can tell its script not to collect any of the User's data. This is a setting that is available in all major browsers.</p>
                            <p>Personal Data collected: Cookies, Usage Data and various types of Data as specified in the privacy policy of the service.</p>
                            <p>Place of processing: Malta – <a href="https://www.hotjar.com/privacy" target="_blank">Privacy Policy</a> – <a href="https://www.hotjar.com/opt-out" target="_blank">Opt Out</a></p>
                        </div>
                    </div>

                    <hr />

                    <h2>Additional information about Data collection and processing</h2>
                    <h3>Legal action</h3>
                    <p>The User's Personal Data may be used for legal purposes by the Data Controller, in Court or in the stages leading to possible legal action arising from improper use of this Application or the related services.
                        The User declares to be aware that the Data Controller may be required to reveal personal data upon request of public authorities.</p>
                    <h3>Additional information about User's Personal Data</h3>
                    <p>In addition to the information contained in this privacy policy, this Application may provide the User with additional and contextual information concerning particular services or the collection and processing of Personal Data upon request.</p>
                    <h3>System logs and maintenance</h3>
                    <p>For operation and maintenance purposes, this Application and any third party services may collect files that record interaction with this Application (System logs) or use for this purpose other Personal Data (such as IP Address).</p>
                    <h3>Information not contained in this policy</h3>
                    <p>More details concerning the collection or processing of Personal Data may be requested from the Data Controller at any time. Please see the contact information at the beginning of this document.</p>
                    <h3>The rights of Users</h3>
                    <p>Users have the right, at any time, to know whether their Personal Data has been stored and can consult the Data Controller to learn about their contents and origin, to verify their accuracy or to ask for them to be supplemented, cancelled, updated or corrected, or for their transformation into anonymous format or to block any data held in violation of the law, as well as to oppose their treatment for any and all legitimate reasons. Requests should be sent to the Data Controller at the contact information set out above.
                    </p><p>This Application does not support “Do Not Track” requests.
                    To determine whether any of the third party services it uses honor the “Do Not Track” requests, please read their privacy policies.</p>
                    <h3>Changes to this privacy policy</h3>
                    <p>The Data Controller reserves the right to make changes to this privacy policy at any time by giving notice to its Users on this page. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom. If a User objects to any of the changes to the Policy, the User must cease using this Application and can request that the Data Controller remove the Personal Data. Unless stated otherwise, the then-current privacy policy applies to all Personal Data the Data Controller has about Users.</p>
                    <h3>Information about this privacy policy</h3>
                    <p>The Data Controller is responsible for this privacy policy.</p>

                    <hr />

                    <h2>Definitions and legal references</h2>
                    <h3>Personal Data (or Data)</h3>
                    <p>Any information regarding a natural person, a legal person, an institution or an association, which is, or can be, identified, even indirectly, by reference to any other information, including a personal identification number.</p>
                    <h3>Usage Data</h3>
                    <p>Information collected automatically from this Application (or third party services employed in this Application), which can include: the IP addresses or domain names of the computers utilized by the Users who use this Application, the URI addresses (Uniform Resource Identifier), the time of the request, the method utilized to submit the request to the server, the size of the file received in response, the numerical code indicating the status of the server's answer (successful outcome, error, etc.), the country of origin, the features of the browser and the operating system utilized by the User, the various time details per visit (e.g., the time spent on each page within the Application) and the details about the path followed within the Application with special reference to the sequence of pages visited, and other parameters about the device operating system and/or the User's IT environment.</p>
                    <h3>User</h3>
                    <p>The individual using this Application, which must coincide with or be authorized by the Data Subject, to whom the Personal Data refers.</p>
                    <h3>Data Subject</h3>
                    <p>The legal or natural person to whom the Personal Data refers.</p>
                    <h3>Data Processor (or Data Supervisor)</h3>
                    <p>The natural person, legal person, public administration or any other body, association or organization authorized by the Data Controller to process the Personal Data in compliance with this privacy policy.</p>
                    <h3>Data Controller (or Owner)</h3>
                    <p>The natural person, legal person, public administration or any other body, association or organization with the right, also jointly with another Data Controller, to make decisions regarding the purposes, and the methods of processing of Personal Data and the means used, including the security measures concerning the operation and use of this Application. The Data Controller, unless otherwise specified, is the Owner of this Application.</p>
                    <h3>This Application</h3>
                    <p>The hardware or software tool by which the Personal Data of the User is collected.</p>
                    <h3>Cookies</h3>
                    <p>Small piece of data stored in the User's device.</p>
                    <h3>Legal information</h3>
                    <p>Notice to European Users: this privacy statement has been prepared in fulfillment of the obligations under Art. 10 of EC Directive n. 95/46/EC, and under the provisions of Directive 2002/58/EC, as revised by Directive 2009/136/EC, on the subject of Cookies.</p>
                    <p>This privacy policy relates solely to this Application.</p>
                    <hr />
                    <p>Latest update: November 27, 2017</p>
                </div>
            </section>
        );
    }

}
