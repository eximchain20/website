import React from "react";
import SubFooter from "./common/SubFooter";


export default class DocumentsPage extends React.Component {

    render(){

        return(
            <div>
                <section className="fdb-block">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 text-left">
                                <h1>Documents</h1>
                                <p className="text-h3">White papers, and other technical and business documents.</p>
                            </div>
                        </div>

                        <div className="row text-left mt-5">
                            <div className="col-12 col-sm-8 col-md-4 col-lg-3 m-sm-auto mr-md-auto ml-md-0">
                              <div className="fdb-box fdb-touch">
                                <img alt="White Paper" className="img-fluid" src="./imgs/documents/white-paper.svg" />
                                <h3><strong>White Paper</strong></h3>
                                <a href="https://www.eximchain.com/Whitepaper%20-%20Eximchain.pdf" download>Download</a>
                              </div>
                            </div>

                            <div className="col-12 col-sm-8 col-md-4 col-lg-3 m-sm-auto pt-5 pt-md-0">
                              <div className="fdb-box fdb-touch">
                                <img alt="Exim Chain Presentation" className="img-fluid" src="./imgs/documents/exim-presentation.svg" />
                                <h3><strong>Presentation</strong></h3>
                                <a href="https://www.eximchain.com/Presentation%20-%20Eximchain.pdf" download>Download</a>
                              </div>
                            </div>

                            <div className="col-12 col-sm-8 col-md-4 col-lg-3 m-sm-auto ml-md-auto mr-md-0 pt-5 pt-md-0">
                              <div className="fdb-box fdb-touch">
                                <img alt="Executive Summary" className="img-fluid" src="./imgs/documents/executive-summary.svg" />
                                <h3><strong>Executive Summary</strong></h3>
                                <a href="https://www.eximchain.com/Executive%20Summary%20-%20Eximchain.pdf" download>Download</a>
                              </div>
                            </div>

                            <div className="col-12 col-sm-8 col-md-4 col-lg-3 m-sm-auto ml-md-auto mr-md-0 pt-5 pt-md-0 hide">
                              <div className="fdb-box fdb-touch">
                                <img alt="Press Kit" className="img-fluid" src="./imgs/documents/press-kit.svg" />
                                <h3><strong>Press Kit</strong></h3>
                                <a href="Press Kit - Eximchain.pdf" download>Download</a>
                              </div>
                            </div>
                        </div>
                    </div>
                </section>
                <SubFooter/>
            </div>
        );

    }
}
