import React, { Component } from 'react';
import Confetti from './reComponents/Confetti'
import { graphql, gql, compose} from 'react-apollo';
//import { InMemoryCache } from 'apollo-cache-inmemory';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import auth0 from 'auth0-js'
import { ToastContainer, toast } from 'react-toastify';
 import { css } from 'glamor';

class Loading extends Component{
  constructor(props) {
    super(props);

    this._webAuth = new auth0.WebAuth({
      audience: 'http://localhost:8080/',
      clientID: 'quxt8iaDfv3lrscQv1mlMKRyJmb9vK26',
      domain: 'eximchain.auth0.com',
      redirectUri: 'http://localhost:3000/loading',
      responseType: 'token',
      scope: 'openid email'
    });

    //use Auth0 token to authenticate with to qraphql microservices\
    this._authorize = (callback) => {
      this._webAuth.parseHash(function(error, authResult) {
        //this._notify(error.errorDescription)
        if (error){
          if (window.localStorage.getItem('auth0AccessToken')){

          } else{
            toast.info("Unauthorized you either failed KYC or never whitelisted, if you think you are receiving this message in error try logging in again or contact whitelist@eximchain.com to inquire the reason for failing KYC, resubmission however will not be allowed", { autoClose: 150000 })
            localStorage.removeItem('graphCoolAccessToken')
            localStorage.removeItem('uid')
            localStorage.removeItem('auth0AccessToken')
            return callback("failed to parse hash", null);

          }

        };
        if (authResult && authResult.accessToken) {
          window.localStorage.setItem('auth0AccessToken', authResult.accessToken)
          return callback(null, authResult.accessToken);
        }
      }.bind(this))


    }
  }


  _accessDenied1 = () => toast.info("Attempting to authenticate please wait", { autoClose: 1500 });
  _accessDenied2 = () => toast.info("Checking IP", { autoClose: 1600 });
  _accessDenied3 = () => toast.info("Attempting to authorize", { autoClose: 1700 });
  _accessDenied4 = () => toast.warn("YOU WILL ONLY BE ABLE TO CONTINUE IF YOU PASS KYC", { autoClose: 1800 });
  _accessDenied5 = () => toast.warn("IF YOU ARE CAUGHT SPOOFING YOUR IP YOUR LOGIN WILL BE FLAGGED", { autoClose: 150000 });
  _accessConfirmed = () => {
     toast.info("If redirect does not occur automatically click on Proceed to Portal button below", {
       position: toast.POSITION.TOP_RIGHT,
       autoClose: 15000,
       className: css({
          background: "#1f69ff"
        })
     });
     setTimeout(this.props.history.replace('/portal'), 3000);
   }
  _isNotLoggedIn = () => {
    const authorized = window.localStorage.getItem('graphCoolAccessToken')
    return  authorized?false:true
  };

  static propTypes = {
    history: PropTypes.object.isRequired,
  };

  _callbackToAuthorizePromise(method, ...args) {
     return new Promise(function(resolve, reject) {
         return method(...args, function(error, result) {
             //if error in storage read reject, result has auth0accessToken to pass into authenticateuser
             //this._notify()
             //this._notify(error.errorDescription)

             if (error) return reject({ok:false,err:error});
             //authenticate and authorize
             this._accessDenied1()
             this._accessDenied2()
             this._accessDenied3()
             this._accessDenied4()
             const variables = {
               accessToken: result,
             };
             this.props
               .authenticateUser({ variables })
               .then(res => {
                 window.localStorage.setItem('graphCoolAccessToken', res.data.authenticateUser.token)
                 window.localStorage.setItem('uid', res.data.authenticateUser.id)
                 this._accessConfirmed()
                 return resolve({ok:true,err:null});
               })
               .catch(error => {
                 return reject({ok:false,err:error});
               });
         }.bind(this));//bind componenet scope to callback
     }.bind(this));//bind component scope to promise
  }//wrap async call inside a promise to await it
  state = {
      on: false
  }
  toggle = () => {
    this.setState({ on: !this.state.on })
  }
  authorize={};
  async _oAuth(){

    //let parse = await this._callbackToParsePromise(this._parseToken);
    try{
      this.authorize = await this._callbackToAuthorizePromise(this._authorize);
      if (this.authorize.ok ==true) {
        this._accessConfirmed()
      }
    }catch(error){
      if(this._isNotLoggedIn()){

          this._accessDenied5()
      }else{

      }

    };



  }//wrap the async calls effect the post condition

  //first time it loads
  //componentDidMount() {

  //  this._oAuth();

  //}
  componentDidUpdate(){
    this._oAuth();
  }
  // shouldComponentUpdate(){
  //   return (this.props.user.user)?false:true
  // }
  //when it changes
  container ={

      //padding:'15%'
  }
  jwtBanner ={
      position: 'relative',
      zIndex: '10',
      color: '#1f69ff',
      height:'25%'
  }
  button ={
      position: 'relative',
      zIndex: '10'
  }
    render(){
        return (
          <section className="fdb-block fdb-block-body">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                  <div className="fdb-box fdb-touch" id="whitelist-confirmation">
                    <div className="row">
                      <div className="col text-center">
                        <h1 className="mb-0"><span class="eximchain-logo darkblue-text"><span>Exim</span>chain</span> <span class="blue-text dinRound-light">KYC</span></h1>
                        <img alt="image" className="img-fluid" style={{maxWidth: 150}} src="./imgs/security.svg" />
                        <h2 className="mt-0">Approved Applicants Only</h2>
                        <p className="text-h4 pb-5">Only applicants that have passed our Know Your Customer process have access to our portal.</p>
                      </div>
                    </div>
                    <div className="row justify-content-center">
                        <a className="btn btn-shadow" href="/">Back to Homepage</a>
                        <a className="btn btn-shadow btn-white" href="/portal">Go to Portal</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <ToastContainer />
          </section>

        );
    }

}

//make user query will fail if no valid key in jwt Middleware
const USER_QUERY = gql`query userQuery {
    user {
      id
      createdAt
      email
    }
  }
`;


const AUTHENTICATE_USER_MUTATION = gql`
  mutation AuthenticateUser($accessToken: String!) {
    authenticateUser(accessToken: $accessToken) {
      id
      token
    }
  }
`;

export default compose(
       graphql(USER_QUERY, {
         name: "user",
         options: { fetchPolicy: 'cache-and-network',ssr: false}
       }),
    )(
      graphql(AUTHENTICATE_USER_MUTATION, {
          name: 'authenticateUser'
        })(
          withRouter(Loading)
        )
);
