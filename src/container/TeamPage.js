import React, { Component } from 'react';
import SubFooter from "./common/SubFooter";

export default class TeamPage extends Component{

    render(){
        return (
            <div>

                <section className="fdb-block team-8" id="team">
                    <div className="container">
                        <div className="row text-center justify-content-center">
                            <div className="col-8">
                                <h1>Meet Our Team</h1>
                                <p className="text-h3">Eximchain was founded in 2015 at MIT. Since then, its founders have been building blockchain-enabled tools to transform the global supply chain by integrating SMEs and increasing transparency.</p>
                            </div>
                        </div>

                        <div className="row-100" />

                        <div className="row justify-content-center text-left">
                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Hope" className="img-fluid" src="./imgs/team/hope.png" />
                                        <div className="col-7">
                                            <h3><strong className="blue-text">Hope Liu</strong></h3>
                                            <p className="text-h4">Co-Founder & CEO</p>
                                            <h3>
                                                <a href="https://www.linkedin.com/in/hope-liu-9ab4a2101/" target="_blank" rel="noopener noreferrer">
                                                    <i className="fa fa-linkedin" />
                                                </a>
                                            </h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>B.A. from Peking University and MBA from MIT. Hope handled cross-border transactions at UBS Asia for 6.5 years. She is the Lab Lead of the North America Blockchain Association and has been working on Eximchain from the MIT Media Lab since 2015.</p>
                                        </div>
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Juan" className="img-fluid" src="./imgs/team/juan.png" />
                                        <div className="col-7">
                                            <h3><strong className="blue-text">Juan Sebastian Huertas</strong></h3>
                                            <p className="text-h4">Co-Founder & CTO</p>
                                            <h3><a href="https://www.linkedin.com/in/juan-huertas/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>B.S. in Computer Science from MIT. Juan started coding at age 13 and worked as a technology consultant for a number of startups. He built a cryptocurrency-enabled game to play and distribute cryptocurrency anonymously during his junior year in college at MIT.</p>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row justify-content-center text-left">
                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Jian" className="img-fluid" src="./imgs/team/jian.png" />
                                        <div className="col-7">
                                            <h3><strong className="blue-text">Jian Xu</strong></h3>
                                            <p className="text-h4">Business Architect</p>
                                            <h3><a href="https://www.linkedin.com/in/jian-xu-1720087/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>
                                        <div className="col-sm-12 pt-3">
                                            <p>James worked at IBM for 14 years holding various positions such as Enterprise Packaged Software Offering Manager, Delivery Project Executive, and Associate Partner managing key accounts in China.</p>
                                        </div>
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Jia" className="img-fluid" src="./imgs/team/jia.png" />
                                        <div className="col-7">
                                            <h3><strong className="blue-text">Jia Zhang</strong></h3>
                                            <p className="text-h4">Business Analyst</p>
                                            <h3><a href="https://www.linkedin.com/in/jia-z-b0a146154/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                                        </div>
                                        <div className="col-sm-12 pt-3">
                                            <p>Jia Zhang has been in the supply chain field since 1994. She spent 7 years in ICBC managing global trade finance and international settlement. After that, she acted as the Chief Representative of MS Textiles in China for almost 10 years, managing local supplier relationships, goods inspection, and logistics arrangement. She is fluent in Chinese, English and French. </p>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row justify-content-center text-left">
                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Louis" className="img-fluid" src="./imgs/team/louis.png" />

                                        <div className="col-7">
                                            <h3><strong className="blue-text">Louis Lamia</strong></h3>
                                            <p className="text-h4">Director of Engineering</p>
                                            <h3><a href="https://www.linkedin.com/in/louis-lamia/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>M.Eng. in Computer Science from MIT. Louis worked for Amazon Web Services for over 2 years on the Elastic File System team, helping to launch the service and working on various features, most notably encryption-at-rest. He has also been involved in teaching introductory programming courses at MIT.</p>
                                        </div>
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Doug" className="img-fluid" src="./imgs/team/doug.png" />

                                        <div className="col-7">
                                            <h3><strong className="blue-text">Douglas Sanchez</strong></h3>
                                            <p className="text-h4">Director of Product</p>
                                            <h3><a href="https://www.linkedin.com/in/dougos/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>After earning his B.S. in Engineering, Douglas assisted in teaching product engineering processes to senior undergradute students at MIT. Most recently he lead design at a manufacturing digitization startup from pre-seed round through series A. He continues to mentor students in product design at MIT.</p>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row justify-content-center text-left">
                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Louis" className="img-fluid" src="./imgs/team/fran.png" />

                                        <div className="col-7">
                                            <h3><strong className="blue-text">Fransheska Colon</strong></h3>
                                            <p className="text-h4">Lead App Developer</p>
                                            <h3><a href="https://www.linkedin.com/in/fransheska-colon-700a0658/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>Fran earned a B.S. in Computer Science and Comparative Media Studies from MIT. After graduating, she worked as a software engineer for a business phone system provider, OnSIP, in New York City, where she developed web, desktop, and mobile applications.</p>
                                        </div>
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="row align-items-center">
                                    <img alt="Doug" className="img-fluid" src="./imgs/team/andrew.png" />

                                        <div className="col-7">
                                            <h3><strong className="blue-text">Andrew Koh</strong></h3>
                                            <p className="text-h4">Lead Solidity Developer</p>
                                            <h3><a href="https://www.linkedin.com/in/andrew-koh-a0276598/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a></h3>
                                        </div>

                                        <div className="col-sm-12 pt-3">
                                            <p>Andrew is currently a Computer Science undergraduate at MIT who has been working within the blockchain space for a couple years. During his tenure at MIT, he has been actively involved with MIT’s Bitcoin Club and MIT Media Lab’s Digital Currency Initiative. He has experience in building Dapp's, or distributed applications, and is excited to develop them on the Eximchain’s Blockchain.</p>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div className="row-70" />

                    </div>
                </section>

                <section className="fdb-block team-5">
                    <div className="container">
                        <div className="row text-center justify-content-center">
                            <div className="col-8">
                                <h1>Advisors</h1>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row text-center justify-content-center">
                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Ramble" className="img-fluid" src="./imgs/advisors/ramble.png" />

                                    <h3><strong className="blue-text">Ramble Lan</strong></h3>
                                    <p>CEO of SwftCoin</p>
                                    <h3><a href="http://www.swftcoin.com" target="_blank" rel="noopener noreferrer"><i className="fa fa-globe"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Tiger" className="img-fluid" src="./imgs/advisors/tiger.png" />

                                    <h3><strong className="blue-text">Tiger Zhong</strong></h3>
                                    <p>CEO of Trade Manager</p>
                                    <h3><a href="http://www.Trademanager.com" target="_blank" rel="noopener noreferrer"><i className="fa fa-globe"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Catherine Dai" className="img-fluid" src="./imgs/advisors/dai.png" />
                                    <h3><strong className="blue-text">Catherine Dai</strong></h3>
                                    <p>Founder of BoaoTech</p>
                                    <h3><a href="https://www.linkedin.com/in/catherine-david-51bb2228/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Daniel Wang" className="img-fluid" src="./imgs/advisors/daniel.png" />

                                    <h3><strong className="blue-text">Daniel Wang</strong></h3>
                                    <p>Dir. Investment at Geely Group</p>
                                    <h3><a href="https://www.linkedin.com/in/daniel-he-wang-276a9311/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>
                        </div>

                        <div className="row justify-content-center text-center">
                            <div className="col-sm-2  m-sm-auto">
                                <img alt="Manmeet Singh" className="img-fluid" src="./imgs/advisors/manmeet.png" />

                                    <h3><strong className="blue-text">Manmeet Singh</strong></h3>
                                    <p>Managing Partner of Blockseed Venture</p>
                                    <h3 className=""><a href="https://www.linkedin.com/in/manmeetsingh/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="William Peckham" className="img-fluid" src="./imgs/team/will.png" />

                                    <h3><strong className="blue-text">William Peckham</strong></h3>
                                    <p>Managing Partner at Proteus Growth</p>
                                    <h3><a href="https://www.linkedin.com/in/william-paul-peckham-b1905a19/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Can Kisagun" className="img-fluid" src="./imgs/advisors/can.png" />

                                    <h3><strong className="blue-text">Can Kisagun</strong></h3>
                                    <p>Co-founder of Enigma</p>
                                    <h3><a href="https://www.linkedin.com/in/cankisagun/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <img alt="Peter Missine" className="img-fluid" src="./imgs/advisors/peter.png" />

                                    <h3><strong className="blue-text">Peter Missine</strong></h3>
                                    <p>Strategy Consultant</p>
                                    <h3><a href="https://www.linkedin.com/in/missine/" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin"></i></a></h3>
                            </div>
                        </div>
                    </div>
                </section>



                <section className="fdb-block team-5 partners">
                    <div className="container">
                        <div className="row text-center justify-content-center">
                            <div className="col-8">
                                <h1>Partners</h1>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row text-center justify-content-center">
                            <div className="col-sm-2 m-sm-auto">
                                <a href="https://www.enuma.io/" target="_blank" rel="noreferrer noopener">
                                    <img alt="Enuma" className="img-fluid" style={{"width": "240px"}} src="./imgs/partners/enuma.png" />
                                </a>

                                {/*<h3><strong className="blue-text">Ramble Lan</strong></h3>
                                <p>President of the North America Blockchain Association</p>*/}
                            </div>

                            <div className="col-sm-2 m-sm-auto">
                                <a href="http://gbic.io/" target="_blank" rel="noreferrer noopener">
                                    <img alt="Gbic" className="img-fluid" style={{"width": "240px"}} src="./imgs/partners/gbic.jpg" />
                                </a>

                                {/*<h3><strong className="blue-text">Tiger Zhong</strong></h3>
                                <p>CEO of Trade Manager</p>*/}
                            </div>
                        </div>
                    </div>
                </section>

                <SubFooter/>
            </div>
        );
    }

}
