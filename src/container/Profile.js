import React, { Component } from 'react';
import { graphql, gql, compose} from 'react-apollo';
import { withRouter,Redirect } from 'react-router-dom';
import Confetti from './reComponents/Confetti'

class Profile extends Component{
  jwtBanner ={
      position: 'relative',
      zIndex: '10',
      color: '#1f69ff'
  }
  shouldComponentUpdate(){
    return (this.props.tge.allTokenSales)?true:true
  }
  _isNotLoggedIn = () => {
    const authenticated = window.localStorage.getItem('auth0AccessToken')
    const authorized = window.localStorage.getItem('graphCoolAccessToken')
    return  authorized?false:true
  };
  render(){
        const email = this.props.user.user?this.props.user.user.email:'loading';
        const tge = this.props.tge.allTokenSales? this.props.tge.allTokenSales[0]:'loading';
        const whiteListDate = this.props.user.user?this.props.user.user.createdAt:'loading';

        if (email==='loading' || tge ==='loading') {
          return this._isNotLoggedIn() ? (<Redirect to="/loading"/>) : (

            <section className="fdb-block blue bg-dark">
              <div className="container">

                  <div className="row text-center">

                      <div className="block-error">
                        <h2 style = {this.jwtBanner}> Checking if Auth key expired or invalid </h2>
                        <h3 style = {this.jwtBanner}> Make sure you are logged in  </h3>
                        <Confetti/>

                      </div>
                  </div>
                  <div className="row justify-content-center">
                      <button className="btn btn-shadow">Back to Homepage</button>
                      <button className="btn btn-shadow btn-white">Previous Page</button>
                  </div>
              </div>
          </section>)

        }

        return (

          this._isNotLoggedIn() ? (<Redirect to="/"/>) : (
            <section className="fdb-block">
              <div className="container">
                  <div className="row justify-content-center">
                            <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                              <div className="mb-4">
                                <h1 className="mb-2"><span class="eximchain-logo darkblue-text"><span>Exim</span>chain</span> <span class="blue-text dinRound-light">KYC</span></h1>
                                <h3 className="token-sale-section-heading">KYC Status</h3>
                                <h4 className="token-sale-section-status">Approved to complete Airdrop Process</h4>
                              </div>
                              <div className="fdb-box fdb-touch fdb-success" id="token-sale">
                                  <div className="text-left">
                                      <div className="row">
                                          <h3 className="col-md-12">Airdrop Data</h3>

                                          <div className="col-md-12">
                                              <div className="row">
                                                  <h5 className="col-md-6">Account Email</h5>
                                                  <h5 className="col-md-6 gray-text">{email}</h5>
                                              </div>
                                              <div className="row">
                                                  <h5 className="col-md-6">Whitelist Confirmed</h5>
                                                  <h5 className="col-md-6 gray-text">{whiteListDate}</h5>
                                              </div>
                                              <div className="row">
                                                  <h5 className="col-md-6">KYC Confirmed</h5>
                                                  <h5 className="col-md-6 gray-text">2018-03-02T18:58:12.000Z</h5>
                                              </div>

                                              <div className="row hide">
                                                  <h5 className="col-md-6">TGE Confirmed</h5>
                                                  <h5 className="col-md-6 gray-text">-</h5>
                                              </div>

                                              <div className="row-70" />

                                              <div className="row">
                                                  <h3 className="col-md-12">Airdrop Next Steps</h3>
                                                  <div className="col-md-12">
                                                    <p>Congratulations for passing our KYC process. We are excited to have you join our network with the ability to participate in governance. Next steps to recieve our Airdrop will be to sign our Airdrop terms agreement. You will receive an email from the Eximchain team when this portal is updated with a link to complete this agreement.</p>
                                                    <br/>
                                                  </div>
                                              </div>

                                              <div className="row begin-kyc hide">
                                                  <div className="col-md-3 ">
                                                      <a className="button btn button btn-shadow pointer" href="#">Begin Airdrop Agreement</a>
                                                  </div>
                                                  <div className="col-md-12">
                                                    <h5>Ready to route to:</h5>
                                                  </div>
                                              </div>

                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div className="block-error">
                                Authenticated as {email}

                              </div>
                      </div>
                  </div>

              </div>
            </section>
        )
    )}

}




//make user query will fail if no valid key in jwt Middleware
const USER_QUERY = gql`query userQuery {
    user {
      id
      createdAt
      email
      auth0UserId
    }
  }
`;


const TOKEN_GENERATION_EVENT_QUERY = gql`query tokenGenerationEventQuery {
  allTokenSales {
    field1
    field2
  }
}`



export default compose(
       graphql(USER_QUERY, {
         name: "user",
         options: { fetchPolicy: 'cache-and-network',ssr: false}
       }),
       graphql(TOKEN_GENERATION_EVENT_QUERY, {
          name: "tge",
          options: { fetchPolicy: 'cache-and-network',ssr: false}
       }),
    )(
          withRouter(Profile)
        );
