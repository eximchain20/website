import React, { Component } from 'react';
import SubFooter from "./common/SubFooter";

export default class RoadMapPage extends Component{

    render(){
        return (
            <div>

                <section className="fdb-block page-block blue" id="road-map">
                    <div className="container">
                        <div className="row">
                            <h1 className="text-center"><span className="eximchain-logo"><span>Exim</span>chain</span> Roadmap</h1>
                            <div className="subheading text-center hide" />
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year">
                                    <img className="icon pull-right hide" src="" alt="Exichain MIT" />
                                        <h4>2015</h4>
                                        <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Can Kisagun develops idea for Eximchain at MIT</h3>
                                </ul>
                            </div>
                            <div className="middle-line first" />
                            <div className="col-md-5 right">
                                <div className="year">
                                    <img className="icon pull-right hide" src="" alt="MIT Medialab" />
                                        <h4 className="text-left">Feburary 2016</h4>
                                        <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Hope Liu and Can Kisagun begin work on Eximchain at MIT Media Lab</h3>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year">
                                    <img className="icon pull-right hide" src="" alt="Engine Innovation Prize" />
                                        <h4>April 2016</h4>
                                        <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Eximchain wins the Engine of Innovation Prize at Rice Business Plan Competition and the Grand Champion of Boston Seagull</h3>
                                </ul>
                            </div>
                            <div className="middle-line" />
                            <div className="col-md-5 right">
                                <div className="year ">
                                    <img className="icon pull-right hide" src="" alt="Eximchain China" />
                                        <h4 className="text-left">June 2016</h4>
                                        <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Hope travels to China to promote Eximchain as <a href="http://legatum.mit.edu/resources/seed-grant-program-developing-world-entrepreneurs/" target="_blank" rel="noopener noreferrer">Legatum Seed Grantee</a></h3>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year ">
                                    <img className="icon pull-right hide" src="" alt="Eximchain legatum" />
                                        <h4>October 2016</h4>
                                        <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Eximchain participates in the <a href="https://www.coindesk.com/plug-and-play-blockchain-fintech-batch-four/" target="_blank" rel="noreferrer noopener">Plug And Play Fintech Accelerator</a> and presents on Demo Day</h3>
                                </ul>
                            </div>
                            <div className="middle-line" />
                            <div className="col-md-5 right">
                                <div className="year">
                                    <h4 className="text-left">December 2016</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Juan Huertas joins Eximchain as CTO</h3>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year ">
                                    <h4>April 2017</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Eximchain named finalist of the <a href="http://mitsloan.mit.edu/newsroom/articles/eight-mit-startups-face-off-wednesday-for-10000/" target="_blank" rel="noreferrer noopener">MIT $100K Accelerate Competition</a></h3>
                                </ul>
                            </div>
                            <div className="middle-line" />
                            <div className="col-md-5 right">
                                <div className="year ">
                                    <h4 className="text-left">June 2017</h4>
                                    <div className="circle now" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Founding team graduates MIT and begins full-time working on Eximchain</h3>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year future">
                                    <h4>July 2017</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <h3>Whitepaper released</h3>
                                </ul>
                            </div>
                            <div className="middle-line" />
                            <div className="col-md-5 right">
                                <div className="year future">
                                    <h4 className="text-left">August - November 2017</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <li>Signed LOIs with 9 companies including SMEs, a listed company in China and a cross-border e-commerce platform</li>
                                    <li>Established a Strategic Cooperation Agreement with Guiyang High-tech Industrial Development Zone Management Commission and Guiyang Big Data Development and Management Commission, China</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year future">
                                    <h4>Q1 2018</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <li>First Proof of Concept in Supply Chain Sourcing</li>
                                    <li>Governance hardening</li>
                                    <li>Large scale network testing</li>
                                    <li>Token Generation Event</li>
                                </ul>
                            </div>
                            <div className="middle-line" />
                            <div className="col-md-5 right">
                                <div className="year future">
                                    <h4 className="text-left">Q2-Q3 2018</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <li>First SDK Release</li>
                                    <li>First Governance Cycle</li>
                                    <li>Network Deployed</li>
                                    <li>Second Proof of Concept in Supply Chain Financing</li>
                                    <li>Mainnet launch</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row flex center">
                            <div className="col-md-5 left">
                                <div className="year future">
                                    <h4>Q4 2018 - Q1 2019</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30">
                                    <li>Second SDK Release</li>
                                    <li>Second Governance Cycle</li>
                                    <li>Third Proof of Concept in Logistics Management</li>
                                </ul>
                            </div>
                            <div className="middle-line"
                                 style={{"borderBottomLeftRadius": "100px", "borderBottomRightRadius": "100px"}} />
                            <div className="col-md-5 right">
                                <div className="year future hide">
                                    <h4 className="text-left">Q2 2019</h4>
                                    <div className="circle" />
                                </div>
                                <ul className="mar-30 hide">
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <SubFooter/>
            </div>
        );
    }

}
