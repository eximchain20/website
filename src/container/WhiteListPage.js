import React, { Component } from 'react';
import SubFooter from "./common/SubFooter";

export default class WhiteListPage extends Component{

    render(){
        return (
            <div>
                <section className="fdb-block">
                    <div className="container">
                        <div className="row text-center align-items-center">
                            <div className="col-12 col-md-6 col-lg-5 ml-auto pt-5 pt-md-0">
                                <img alt="token" className="fdb-icon hide" src="./imgs/img_round.svg" />
                                <h1>
                                    <span className="eximchain-logo darkblue-text">
                                        <span>Exim</span>chain
                                    </span>
                                    <span className="dinRound-light blue-text">Whitelist</span>
                                </h1>
                                <p class="text-h3">
                                  Whitelist is closed!
                                  <br/>
                                  Learn more on our <a href="https://medium.com/eximchain/whitelist-registration-has-closed-a243f71e646e" target="_blank" rel="noreferrer noopener">blog</a>.
                                </p>
                            </div>
                            <div className="col-8 col-md-4">
                                <img alt="whitelist" className="img-fluid" src="./imgs/whitelist.svg" />
                            </div>
                            <div className="col-4 col-md-2">
                                <div className="row">
                                    <div className="col-12">
                                        <img alt="pool" className="img-fluid" src="./imgs/pool.svg" />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-12">
                                        <img alt="wallet" className="img-fluid" src="./imgs/wallet.svg" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <SubFooter/>
            </div>
        );
    }

}
