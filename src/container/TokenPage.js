import React, { Component } from 'react';
import SubFooter from "./common/SubFooter";

export default class TokenPage extends Component{

    render(){
        return (
            <div>
                <section className="fdb-block">
                    <section className="container container-token-allocation">
                        <div className="row justify-content-center pb-5">
                            <div className="col-12 text-center">
                                <h1 className="header"><span className="eximchain-logo"><span>Exim</span>chain</span> Token: <strong>EXC</strong></h1>
                            </div>
                            <div className="col text-center col-sm-12">
                                <img alt="Exim Chain Logo" className="img-fluid" src="./imgs/exc.svg"
                                     style={{"maxWidth": "120px", "marginLeft": "auto", "marginRight": "auto"}} />
                            </div>
                        </div>
                        <div className="row justify-content-center pb-3">
                            <div className="col col-md-8 text-center">
                                <h2><strong>Token Generation Event</strong><br />Q1 2018</h2>
                                <h3 className="text-h3">Total Supply: 150 million EXC</h3>
                                <h3 className="text-h3">Fundraising Cap: $20 million</h3>
                            </div>
                        </div>
                        <div className="row text-left pt-4">
                            <div className="col-12 col-md-6">
                                <p className="text-h4">EXC tokens are ERC-20 compatible, and their purpose is to generate a valid ledger to seed the genesis file of our Eximchain mainnet. The ERC-20 tokens will be distributed on the Ethereum blockchain pursuant to a related ERC-20 smart contract. They are redeemable for <strong>Native Tokens</strong> on the Eximchain Platform upon network launch.</p>
                            </div>
                            <div className="col-12 col-md-6">
                                <p className="text-h4">Following the Eximchain Mainnet Network Launch in Q2 2018, Eximchain’s <strong>Native Tokens</strong> will be used to pay network fees, validate state changes, and execute governance. The <strong>Native Tokens</strong> will also be used to access applications built on the Eximchain network, such as Smart Contracts and solutions developed with the Eximchain SDK.</p>
                            </div>
                        </div>
                    </section>
                </section>
                <section className="fdb-block">
                    <div className="row">
                        <div className="col-12 text-center">
                            <h2 className="header">Token Allocation</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col text-center col-sm-12">
                            <img alt="Funds" className="img-fluid mt-5" src="./imgs/funds.svg"
                                 style={{"maxWidth": "500px", "marginLeft": "auto", "marginRight": "auto"}} />
                        </div>
                    </div>
                </section>

                <section className="fdb-block">
                  <div className="container">
                    <div className="row mt-5 justify-content-center">
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 m-sm-auto mr-md-auto ml-md-0">
                          <div className="fdb-box fdb-touch text-center">
                            <img alt="White Paper" className="img-fluid" src="./imgs/documents/white-paper.svg" />
                            <h3><strong>Token Privacy Policy</strong></h3>
                            <a href="https://www.eximchain.com/Privacy%20Policy%20-%20Eximchain.pdf" download>Download</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                <SubFooter/>
            </div>
        );
    }

}
