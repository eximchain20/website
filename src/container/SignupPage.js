import React from "react";
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';


class SignupPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            errors: {
                non_whitelisted: false,
                password_mismatch: false
            },
            email: "",
            password: "",
            confirm_password: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        let email = this.state.email;
        let password = this.state.password;
        if(this.state.password !== this.state.confirm_password){
            this.setState({errors: {password_mismatch: true}});
        }else{
            this.props.mutate({
                variables: { email, password }
            }).then(({ data }) => {
            }).catch((error) => {
                this.setState({errors: {non_whitelisted: error.message.substring("GraphQL error: ".length)}});
            });
        }
    }

    render(){
        let note, passwords_mismatch = "";
        if(this.state.errors.non_whitelisted){
            note = <div className="note red-note">
                {this.state.errors.non_whitelisted}
            </div>
        }else {
            note = <div className="note gray-note">
                Email must have been approved on whitelist
            </div>;
        }
        if(this.state.errors.password_mismatch){
            passwords_mismatch = <div className="note red-note">
                Passwords mismatch
            </div>
        }


        return(
            <div className="fdb-block scof-bkg" id="whitelist-form">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                            <div className="fdb-box fdb-touch">
                                <div className="row">
                                    <div className="col-md-8 offset-md-2 mt-1 text-left">
                                        <h1>Sign up</h1>
                                        <p className="text-h4">EximChain Token Sale</p>
                                    </div>
                                </div>

                                <div id="mc_embed_signup">
                                    <form onSubmit={this.handleSubmit} method="post">
                                        {/*action="https://eximchain.us16.list-manage.com/subscribe/post?u=d54203c31ac2e4ee93e6b6997&amp;id=8dc719f8a9"
                                        method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate=""*/}
                                        <div id="mc_embed_signup_scroll">
                                            <div className="mc-field-group row">
                                                <div className="col-md-8 offset-md-2 mt-1">
                                                    {/*<label htmlFor="mce-EMAILID">Email ID  <span className="asterisk">*</span></label>*/}
                                                    <input type="text" value={this.state.email}
                                                           onChange={(event) => {this.setState({email: event.target.value})}}
                                                           name="EMAILID" className="form-control required"
                                                           id="mce-EMAILID" placeholder={"email"}/>
                                                    {note}
                                                </div>
                                            </div>

                                            <div className="mc-field-group row">
                                                <div className="col-md-8 offset-md-2 mt-1">
                                                    {/*<label htmlFor="mce-PASSWORD">Password  <span className="asterisk">*</span></label>*/}
                                                    <input type="password" value={this.state.password}
                                                           onChange={(event) => {this.setState({password: event.target.value})}}
                                                           name="PASSWORD" className="form-control required"
                                                           id="mce-PASSWORD" placeholder={"password"}/>
                                                </div>
                                            </div>

                                            <div className="mc-field-group row">
                                                <div className="col-md-8 offset-md-2 mt-1">
                                                    {/*<label htmlFor="mce-CONFIRMPASSWORD">Confirm Password  <span className="asterisk">*</span></label>*/}
                                                    <input type="password" value={this.state.confirm_password}
                                                           onChange={(event) => {this.setState({confirm_password: event.target.value})}}
                                                           name="CONFIRMPASSWORD" className="form-control required"
                                                           id="mce-CONFIRMPASSWORD" placeholder={"confirm password"}/>
                                                    {passwords_mismatch}
                                                </div>
                                            </div>

                                            <div style={{position: "absolute", left: "-5000px"}} aria-hidden="true"><input type="text" name="b_d54203c31ac2e4ee93e6b6997_8dc719f8a9" tabIndex="-1" value=""/></div>
                                            <div className="clear row mt-4">
                                                <div className="col">
                                                    {/*<span>
                                                        Already have an account? <a href="/signin">Signin</a>
                                                    </span>*/}
                                                </div>
                                                <div className="col">
                                                    <input type="submit" value="Signup" name="subscribe" id="mc-embedded-subscribe" className="button btn button btn-shadow"/>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }

}

const signupQuery = gql`
    mutation signupUser($email: String!, $password: String!) {
        signupUser(email: $email, password: $password) {
            status
        }
    }`;

export default graphql(signupQuery)(SignupPage);
