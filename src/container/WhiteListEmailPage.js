import React, { Component } from 'react';

export default class WhiteListEmailPage extends Component{

    render(){
        return (
            <section className="fdb-block fdb-block-body">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                            <div className="fdb-box fdb-touch" id="whitelist-confirmation">
                                <div className="row">
                                    <div className="col text-center">
                                        <h1 className="mb-0">
                                            <span className="eximchain-logo darkblue-text">
                                                <span>Exim</span>chain&nbsp;
                                            </span>
                                            <span className="blue-text dinRound-light">Whitelist</span>
                                        </h1>
                                        <img alt="Email" className="img-fluid" style={{"maxWidth": "150px"}} src="./imgs/email.svg" />
                                        <h2 className="mt-0">Confirm Whitelist Email</h2>
                                        <p className="text-h4">We need to confirm your email address. To complete the application process, please click the link in the email we just sent you.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}
