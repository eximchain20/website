import React, { Component } from 'react';
import SubFooter from "./common/SubFooter";


export default class FAQPage extends Component{

    render(){
        return (
            <div>
                <section className="fdb-block">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 text-left">
                                <h1>FAQ</h1>
                                <p className="text-h3 hide">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12">
                                <h2 className="blue-text">Project</h2>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>What is Eximchain?</strong></h3>
                                <p>Eximchain is a software development company focused on supply chain applications. From supplier credit to inventory management, Eximchain helps businesses connect, transact, and share information more efficiently and securely. Eximchain’s mission is to provide blockchain-enabled tools to transform the global supply chain by integrating SMEs and increasing transparency.</p>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0">
                                <h3><strong>Who are Eximchain’s target customers?</strong></h3>
                                <p>Eximchain’s applications are designed to be used by SME manufacturers and retailers.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12">
                                <h2 className="blue-text">Benefits</h2>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>How do Eximchain’s applications increase supply chain efficiency?</strong></h3>
                                <p>Eximchain’s applications use state-of-the-art blockchain technology to ensure accuracy, transparency, and security. They are designed to break down barriers and integrate actors big and small into an inclusive, transparent, and secure global network.</p>
                                <p>Eximchain’s applications streamline processes in areas such as:</p>
                                <ul>
                                    <li>Sourcing: Eximchain securely records historical data and transactions allowing suppliers to prove their reliability to buyers and rating institutions.</li>
                                    <li>Supply Chain Finance: Eximchain Smart Contracts allow financiers to verify the validity of orders placed with all upstream partners and suppliers and provide the necessary financing.</li>
                                    <li>Inventory Management: Eximchain enables partners to seamlessly share demand and inventory information across a common ledger.</li>
                                </ul>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0">
                                <h3><strong>What problem does Eximchain solve?</strong></h3>
                                <p>Shareholders lost over $5.2 trillion in potential revenue last year due to the lack of transparency, connectivity, and agility in the global supply chain. Eximchain applications correct these inefficiencies and are designed to be used by both SMEs and enterprises engaged in logistics and global trade.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12">
                                <h2 className="blue-text">Technology &amp; Product</h2>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>What is a blockchain?</strong></h3>
                                <p>A blockchain is a distributed network that makes use of cryptography to securely host applications, store data, and transfer value.</p>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0">
                                <h3><strong>Why use blockchain technology?</strong></h3>
                                <p>Eximchain uses blockchain technology to digitize and decentralize business processes that have traditionally relied on intermediaries such as brokers and banks. Blockchain enables Eximchain’s applications to create decentralized ledgers, reduce transaction costs, and securely share information and value in real time.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>What is Eximchain’s governance model?</strong></h3>
                                <p>Eximchain’s governance model relies on Quadratic Voting, which is designed to avoid the tyranny of the majority by allowing people to express how strongly they feel about an issue rather than just whether they are in favor of it or opposed to it.</p>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0">
                                <h3><strong>How is Quadratic Voting  used in Eximchain’s governance model?</strong></h3>
                                <p>Participants in the Eximchain governance protocol vote on which nodes conduct consensus for the network through Quadratic Voting. In such a system, V votes cost V2 tokens to cast. At the end of each governance cycle, all tokens cast as votes are redistributed evenly between the voters.</p>
                                <p>The redistribution provides a crypto-economic incentive to participate in network governance. For example, casting one vote for one token guarantees a return of &lt;1 token even if another person chooses to buy more than a single vote as a consensus contributor.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>Why are Eximchain applications built on Quorum?</strong></h3>
                                <p>Combining a Blockchain, SDK, and Platform layer is necessary to bring enough players to bear for a healthy minimum viable network. Faced with the choice of waiting for Ethereum to end the Ice Age and release Casper in the coming years, or building on recently produced viable solutions in this area, Eximchain has chosen to leapfrog the Ethereum Roadmap by leveraging Quorum, an enterprise-focused version of Ethereum released by JP Morgan.</p>
                                <p>Quorum, and therefore Eximchain’s applications, is designed to develop and evolve alongside Ethereum. Because it only minimally modifies Ethereum’s core, Quorum enables Eximchain to incorporate Ethereum updates quickly and seamlessly.</p>
                                <p>At the most basic level, Eximchain modifies how the Ethereum client protocol enables consensus and permissions the blockchain. The Eximchain blockchain distinguishes between participants that can validate blocks and participants that can propose blocks. The latter allows Eximchain to both secure private transactions on the network and incentivize developers to participate in the evolution of the environment.</p>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0 hide">
                                <h3><strong>What is Eximchain’s governance model?</strong></h3>
                                <p>Eximchain’s governance model relies on Quadratic Voting, which is designed to avoid the tyranny of the majority by allowing people to express how strongly they feel about an issue rather than just whether they are in favor of it or opposed to it.</p>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12">
                                <h2 className="blue-text">Token</h2>
                            </div>
                        </div>

                        <div className="row text-left pt-5">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h3><strong>Who can participate?</strong></h3>
                                <p>All interested participants must fill out the Whitelist form and pass a KYC process in order to gain access. US, China, and OFAC sanctioned countries cannot participate in the token generation event. Bounty campaign participants must also fill out the Whitelist form and pass a KYC process in order to receive tokens.</p>
                            </div>

                            <div className="col-12 col-md-6 col-lg-5 ml-md-auto pt-5 pt-md-0">
                              <h3><strong>Can I still join the Whitelist?</strong></h3>
                              <p>We closed our whitelist application at 1pm ET, Jan 15th 2018. We found it necessary to take action and close the whitelist early to protect the interests of our early supporters.</p>
                            </div>
                        </div>

                    </div>
                </section>
                <SubFooter/>
            </div>
        );
    }

}
