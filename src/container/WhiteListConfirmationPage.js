import React, { Component } from 'react';
import Confetti from './reComponents/Confetti';

export default class WhiteListConfirmationPage extends Component{

    render(){
        return (
            <div>
                <section className="fdb-block fdb-block-body" style="background: transparent; z-index: 1;">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                                <div className="fdb-box fdb-touch" id="whitelist-confirmation">
                                    <div className="row">
                                        <div className="col text-center">
                                            <h1 className="mb-0"><span className="eximchain-logo darkblue-text"><span>Exim</span>chain</span> <span className="blue-text dinRound-light">Whitelist</span></h1>
                                            <img alt="image" className="img-fluid" style="max-width: 150px;" src="./imgs/yay.svg" />
                                            <h2 className="mt-0">Whitelist Application Submitted!</h2>
                                            <p className="text-h4">
                                                Your application to our whitelist has been submitted.
                                                <br />
                                                Thank you for applying!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Confetti/>
            </div>
        );
    }

}
