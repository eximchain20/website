import React from 'react';
import SubFooter from "./common/SubFooter";

class IndexPage extends React.Component {

    render() {

        return (
            <div>
                <section className="fdb-block bg-dark" style={{"backgroundImage": "url(./imgs/bg_cargo.svg)"}} id="hero">
                    <div className="container">
                        <div className="row justify-content-start">
                            <div className="col-12 col-sm-12 col-lg-7 text-left">
                                <h1><strong>Powering the Global Supply Chain on Blockchain</strong></h1>
                                <p className="text-h4 dinRound-bold">Pioneering a scalable, public blockchain with privacy for enterprise supply chain applications. Eximchain enables businesses to connect, transact, and share information more efficiently and securely.</p>
                                <p className="mt-5">
                                    <a className="btn btn-white btn-shadow" href="https://t.me/eximchain" target="_blank" rel="noopener noreferrer">
                                        <i className="fa fa-telegram" /> Join us on Telegram
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="fdb-block" id="product">
                    <div className="container justify-content-center align-items-center d-flex">
                        <div className="row justify-content-center text-center">
                            <div className="col-12 col-md-8">
                                <img alt="Innovative solutions" className="fdb-icon hide" src="./imgs/img_round.svg" />
                                <h1>Utility Infrastructure for Enterprise Supply Chain Applications</h1>
                                <p className="text-h4">Eximchain enables businesses to connect, transact, and share information more efficiently and securely through our blockchain utility infrastructure. Using Eximchain blockchain technology, enterprises can eliminate traditional supply chain barriers and integrates actors big and small into an efficient, transparent, and secure global network.</p>
                                <p className="mt-5"><a href="https://eximchain.com/Whitepaper%20-%20Eximchain.pdf" className="btn btn-shadow" download><span className="eximchain-logo"><span>Exim</span>chain</span> Whitepaper</a></p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="fdb-block fdb-viewport bg-dark" style={{"backgroundImage": "url(./imgs/bg_apps.svg)"}} id="solutions">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 text-center">
                                <h1><span className="eximchain-logo"><span>Exim</span>chain</span> Solutions</h1>
                            </div>
                        </div>

                        <div className="row-70" />

                        <div className="row text-center justify-content-sm-center no-gutters">
                            <div className="col-12 col-sm-10 col-md-8 col-lg-7 col-xl-3 m-auto">
                                <div className="fdb-box fdb-touch">
                                    <img alt="Financing" className="img-fluid" src="./imgs/financing.svg" />
                                    <h2>Supply Chain Finance</h2>
                                    <p>Eximchain Smart Contracts allow banks to verify the validity of orders placed with all upstream partners and suppliers and provide the necessary financing.</p>
                                    <p className="mt-4 hide"><a href="https://www.froala.com">Learn More &gt;</a></p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-10 col-md-8 col-lg-7 col-xl-3 m-auto pt-5 pt-xl-0">
                                <div className="fdb-box fdb-touch">
                                    <img alt="Sourcinf" className="img-fluid" src="./imgs/sourcing.svg" />
                                    <h2>Sourcing</h2>
                                    <p>Eximchain Smart Contracts securely record historical data and transactions allowing suppliers to prove their reliability to buyers and rating institutions.</p>
                                    <p className="mt-4 hide"><a href="/sourcing.html">Learn More &gt;</a></p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-10 col-md-8 col-lg-7 col-xl-3 m-auto pt-5 pt-xl-0">
                                <div className="fdb-box fdb-touch">
                                    <img alt="Inventory" className="img-fluid" src="./imgs/inventory.svg" />
                                    <h2>Inventory Management</h2>
                                    <p>Eximchain tools enable partners to seamlessly share demand and inventory information across a common ledger.</p>
                                    <p className="mt-4 hide"><a href="https://www.froala.com">Learn More &gt;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="fdb-block">
                    <div className="container">
                        <div className="row text-center">
                            <div className="col-12">
                                <h1>Embrace the Future</h1>
                            </div>
                        </div>

                        <div className="row text-center justify-content-center mt-5">
                            <div className="col-12 col-sm-4 col-xl-3 m-md-auto">
                                <img alt="Efficiency" className="fdb-icon" src="./imgs/efficiency.svg" />
                                <h3><strong>Efficiency</strong></h3>
                                <p>Reduce the time spent on manual reconciliation and the cost of 3rd party verification through a decentralized network.</p>
                            </div>

                            <div className="col-12 col-sm-4 col-xl-3 m-auto pt-4 pt-sm-0">
                                <img alt="Transparency" className="fdb-icon" src="./imgs/transparency.svg" />
                                <h3><strong>Transparency</strong></h3>
                                <p>Align the interests of partners within supply chains to share information with others to gain visibility.</p>
                            </div>

                            <div className="col-12 col-sm-4 col-xl-3 m-auto pt-4 pt-sm-0">
                                <img alt="Security" className="fdb-icon" src="./imgs/security.svg" />
                                <h3><strong>Security</strong></h3>
                                <p>Secure sensitive data through cryptography and network security through our unique consensus protocol + governance mechanism.</p>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-12">
                                <img alt="World" className="img-fluid" src="./imgs/world.svg" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="fdb-block" id="sdk">
                    <div className="container">
                        <div className="row justify-content-center pb-xl-5">
                            <div className="col-12 col-md-9 text-center">
                                <h1><span className="eximchain-logo"><span>Exim</span>chain</span> Software Development Kit</h1>
                                <p className="text-h3">The Eximchain SDK enables companies to quickly build customized, end-to- end, supply chain applications with data privacy.</p>
                            </div>
                        </div>

                        <div className="row text-right align-items-center-lg align-items-end pt-5">
                            <div className="col-7 col-sm-4 m-auto mt-md-0 m-lg-auto">
                                <img alt="Smart Contracts" className="img-fluid br-b-0" src="./imgs/smart-contracts.svg" />
                            </div>

                            <div className="col-12 col-md-7 col-lg-6 col-xl-5 m-auto text-left pt-5 pt-md-0">
                                <h3><strong>Supply Chain Finance, Logistics, and Sourcing Modules</strong></h3>
                                <p className="hide">The smart contract SDK layer will allow developers to build applications from basic UX + Smart Contract components, accelerating development of future Supply Chain Finance, Logistics, and Sourcing solutions.</p>

                                <h3 className="mt-4 mt-xl-5"><strong>Private Trade Negotiation</strong></h3>
                                <p className="hide">Private negotiation management, preserving the privacy of the parties to the negotiation, and the confidentiality of the negotiations business logic and settlement agreement.</p>

                                <h3 className="mt-4 mt-xl-5"><strong>ZSL Shielded Transactions</strong></h3>
                                <p className="hide">Private negotiation settlement, transfer of digital assets on-chain reveals no information about the Sender, Recipient, or the quantity of assets that are being transferred.</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="fdb-block">
                    <div className="container">
                        <div className="row text-center">
                            <div className="col-12">
                                <h2 className="hide">In the News</h2>

                                <div className="mt-5 justify-content-center">
                                    <a href="https://www.coindesk.com/plug-and-play-blockchain-fintech-batch-four/" target="_blank" rel="noopener noreferrer">
                                        <img alt="Coin Desk" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/coindesk.png" />
                                    </a>
                                    <a href="https://bitcoinmagazine.com/articles/pnp-fintech-startup-program-may-signal-industry-shift-from-blockchain-back-to-bitcoin-1471886420/" target="_blank" rel="noopener noreferrer">
                                        <img alt="BitCoin" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/bitcoin.png" />
                                    </a>
                                    <a href="https://mp.weixin.qq.com/s?__biz=MzA3MzI4MjgzMw==&mid=2650719265&idx=2&sn=def3ff299a5d0bcad8c55531bb1205b2&chksm=871b005fb06c8949721cc25378d0c3ea71cf3b1208f07984c2bf08f204e188bfe2684f40e2a4&scene=0" target="_blank" rel="noopener noreferrer">
                                        <img alt="China" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/china.jpg" />
                                    </a>
                                    <a href="https://ny.uschinapress.com/weekends/2016/04-22/93241.html" target="_blank" rel="noopener noreferrer">
                                        <img alt="China Press" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/chinapress.png" />
                                    </a>
                                    <a href="https://mitsloan.mit.edu/newsroom/articles/eight-mit-startups-face-off-wednesday-for-10000/" target="_blank" rel="noopener noreferrer">
                                        <img alt="Slogan" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/sloan.png" />
                                    </a>
                                </div>
                                <div className="mt-2 mt-md-5 justify-content-center">
                                    <a href="https://www.bizjournals.com/houston/morning_call/2016/04/texas-a-m-company-takes-top-prize-at-2016-rice.html" target="_blank" rel="noopener noreferrer">
                                        <img alt="Rice" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/rice.png" />
                                    </a>
                                    <a href="https://laireastlabs.com/" target="_blank" rel="noopener noreferrer">
                                        <img alt="Lair" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/lair.png" />
                                    </a>
                                    <img alt="100k" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/100k.png" />
                                    <img alt="OTEC" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/otec.png" />
                                    <img alt="SandBox" height="30" className="ml-3 mr-3 mb-2 mt-2" src="./imgs/news/sandbox.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <SubFooter/>

            </div>
        );
    }
}

// export default graphql(query)(IndexPage);

//TODO remove this once gql is enabled and enable the above line
export default IndexPage;
