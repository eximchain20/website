import React, { Component } from 'react';

export default class FiveOO extends Component{

    render(){
        return (
            <section className="fdb-block blue bg-dark">
                <div className="container">
                    <div className="row text-center">
                        <div className="block-error">
                            <div className="error-num">500</div>
                            <div className="error-text">Internal Server Error</div>

                            <div className="error-description">Unfortunately we're having trouble loading the page you are looking for. Please wait a moment and try again or use action below.</div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <button className="btn btn-shadow" onclick="document.location.href = 'index.html';">Back to Homepage</button>
                        <button className="btn btn-shadow btn-white" onclick="history.back();">Previous Page</button>
                    </div>
                </div>
            </section>
        );
    }

}
