import React, { Component } from 'react';
import Confetti from './reComponents/Confetti';


export default class NewsletterConfirmationPage extends Component{

    render(){

        return (
            <div>
                <section className="fdb-block fdb-block-body" style={{"background": "transparent", "zIndex": "1"}}>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                                <div className="fdb-box fdb-touch" id="whitelist-confirmation">
                                    <div className="row">
                                        <div className="col text-center">
                                            <h1 className="mb-0">
                                                <span className="eximchain-logo darkblue-text">
                                                    <span>Exim</span>chain&nbsp;
                                                </span>
                                                <span className="blue-text dinRound-light">Newsletter</span>
                                            </h1>
                                            <img alt="Email" className="img-fluid" style={{"maxWidth": "150px"}} src="./imgs/email.svg" />
                                            <h2 className="mt-0">Newsletter Subscription Confirmed!</h2>
                                            <p className="text-h4">Your subscription to our newsletter has been confirmed. Thank you for subscribing!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Confetti/>
            </div>
        );
    }

}
