import React from "react";
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';


class SigninPage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            errors: {
                invalid_credentials: false
            },
            email: "",
            password: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        let email = this.state.email;
        let password = this.state.password;
        this.props.mutate({
            variables: { email, password }
        }).then(({ data }) => {

        }).catch((error) => {
            this.setState({errors: {invalid_credentials: error.message.substring("GraphQL error: ".length)}});
        });
    }

    render(){

        let err = "";
        if(this.state.errors.invalid_credentials){
            err = <div className="note red-note">
                {this.state.errors.invalid_credentials}
            </div>;
        }

        return(
            <div className="fdb-block" id="whitelist-form">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-8 col-lg-7 col-md-5 text-center">
                            <div className="fdb-box fdb-touch">
                                <div className="row">
                                    <div className="col text-center">
                                        <h1>Signin to exim chain</h1>
                                        <p className="text-h4">Signin to exim chain</p>
                                    </div>
                                </div>

                                <div id="mc_embed_signup">
                                    <form onSubmit={this.handleSubmit} method="post" >
                                        {/*action="https://eximchain.us16.list-manage.com/subscribe/post?u=d54203c31ac2e4ee93e6b6997&amp;id=8dc719f8a9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate=""*/}
                                        <div id="mc_embed_signup_scroll">

                                            <div className="mc-field-group row">
                                                <div className="col mt-1">
                                                    <label htmlFor="mce-EMAILID">Email ID  <span className="asterisk">*</span></label>
                                                    <input type="text" name="EMAILID"
                                                           value={this.state.email}
                                                           onChange={(event) => {this.setState({email: event.target.value})}}
                                                           className="form-control required" id="mce-EMAILID"/>
                                                </div>
                                            </div>

                                            <div className="mc-field-group row">
                                                <div className="col mt-1">
                                                    <label htmlFor="mce-PASSWORD">Password  <span className="asterisk">*</span></label>
                                                    <input type="password" name="PASSWORD"
                                                           value={this.state.password}
                                                           onChange={(event) => {this.setState({password: event.target.value})}}
                                                           className="form-control required" id="mce-PASSWORD"/>
                                                    {err}
                                                </div>
                                            </div>

                                            <div style={{position: "absolute", left: "-5000px"}} aria-hidden="true"><input type="text" name="b_d54203c31ac2e4ee93e6b6997_8dc719f8a9" tabIndex="-1" value=""/></div>
                                            <div className="clear row mt-4">
                                                <div className="col">
                                                    <input type="submit" value="Signin" name="subscribe" id="mc-embedded-subscribe" className="button btn button btn-shadow"/>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }

}

const signinQuery = gql`
    mutation signinUser($email: String!, $password: String!) {
        signinUser(email: $email, password: $password) {
            status
        }
    }`;

export default graphql(signinQuery)(SigninPage);
