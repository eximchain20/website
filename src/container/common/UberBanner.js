import React from 'react';

export default class UberBanner extends React.Component {
    render() {
        return (
            <div className="text-center uber-banner hidden-xs">
                The Eximchain Whitelist is closed!
                Join us on <a href="https://t.me/eximchain" target="_blank" rel="noreferrer noopener" title="Join our Telegram channel">Telegram</a> to learn more.
            </div>
        );
    }
}
