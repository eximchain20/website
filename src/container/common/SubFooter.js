import React from 'react';

export default class SubFooter extends React.Component {
    render() {
        return (
            <section className="fdb-block">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
                            <h1>Join our community!</h1>
                            <p className="text-h3">Sign up for updates about our upcoming token generation event.</p>
                            {/*Begin MailChimp Signup Form*/}
                            <div id="mc_embed_signup">
                                <form action="https://eximchain.us16.list-manage.com/subscribe/post?u=d54203c31ac2e4ee93e6b6997&amp;id=3c24cf4c71" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate="">
                                    <div id="mc_embed_signup_scroll" className="input-group mt-5 mb-5">
                                        <input type="email" name="EMAIL" className="email form-control" id="mce-EMAIL" placeholder="Enter you email address" required="" />
                                            {/*real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}
                                            <div style={{position: "absolute", left: -5000+"px"}} aria-hidden="true">
                                                <input type="text" name="b_d54203c31ac2e4ee93e6b6997_3c24cf4c71" tabIndex="-1" value="" />
                                            </div>
                                            <div className="clear input-group-btn">
                                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="btn button" />
                                            </div>
                                    </div>
                                </form>
                            </div>
                            {/*End mc_embed_signup*/}
                            <p className="text-h2">
                                <a href="https://medium.com/eximchain" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-medium"></i>
                                </a>&nbsp;&nbsp;&nbsp;
                                <a href="https://twitter.com/EximchainEXC" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-twitter"></i>
                                </a>&nbsp;&nbsp;&nbsp;
                                <a href="https://www.linkedin.com/company/11194001/" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-linkedin"></i>
                                </a>&nbsp;&nbsp;&nbsp;
                                <a href="https://www.facebook.com/eximchain/" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-facebook"></i>
                                </a>&nbsp;&nbsp;&nbsp;
                                <a href="https://github.com/eximchain" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-github"></i>
                                </a>&nbsp;&nbsp;&nbsp;
                                <a href="https://t.me/eximchain" target="_blank" rel="noopener noreferrer">
                                    <i className="fa fa-telegram"></i>
                                </a>
                            </p>

                            <a href="mailto:hello@eximchain.com" target="_top"><h3 className="mt-4">hello@eximchain.com</h3></a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
