import React from 'react';

export default class HoveringTelegramButton extends React.Component {
    render() {
        return (
            <div id="telegram-button">
                <a target="_blank" href="https://t.me/eximchain" rel="noopener noreferrer">
                    <div className="circle">
                        <img src="./imgs/telegram.svg" alt=""/>
                    </div>
                </a>
            </div>
        );
    }
}