import React from 'react';

export default class Header extends React.Component {
    constructor(props) {
      super(props);
      //Authentication Provider
      this._webAuth = new auth0.WebAuth({
        audience: 'http://localhost:8080/',
        clientID: 'quxt8iaDfv3lrscQv1mlMKRyJmb9vK26',
        domain: 'eximchain.auth0.com',
        redirectUri: 'http://localhost:3000/#portal',
        responseType: 'token',
        scope: 'openid email'
      });
    }
    //static proptypes and css
    static propTypes = {
      authenticateUser: PropTypes.func.isRequired,
      history: PropTypes.object.isRequired
    };

    container = {
      marginTop: '100px'
    };

    loginButton = {
      padding: '10px',
      width: '150px',
      background: '#00BFFF',
      color: 'white',
      cursor: 'pointer',
      border: 'none'
    };


    _logout(){
        localStorage.removeItem('graphCoolAccessToken')
        localStorage.removeItem('uid')
        localStorage.removeItem('auth0AccessToken')
        window.location.reload()
      }



     //login with Auth0 client returns authzero token which we can then use to authenticate with graphcool
     _authenticate(){
        this._webAuth.authorize()
        window.location.reload()
     }
     //parse the token
    _parseToken(arg,callback){
      if (window.localStorage.getItem('auth0AccessToken')){
       //great already parse and still loged in
       return callback(null,true);
      }else{
       //parse the token
       this._webAuth.parseHash(function(error, authResult) {
         if (error) return callback(error,false);
         if (authResult && authResult.accessToken) {
           window.localStorage.setItem('auth0AccessToken', authResult.accessToken)
           return callback(null, true);
         }
       }.bind(this));
      }
    }
    //use Auth0 token to authenticate with to qraphql microservices\
    _authorize(arg,callback){
      const variables = {
        accessToken: window.localStorage.getItem('auth0AccessToken'),
      };
      this.props
        .authenticateUser({ variables })
        .then(res => {
          window.localStorage.setItem('graphCoolAccessToken', res.data.authenticateUser.token)
          window.localStorage.setItem('uid', res.data.authenticateUser.id)
          return callback(null, true);
        }.bind(this))
        .catch(error => {
          return callback(error,false);
        });
    }
    _callbackToPromise(method, ...args) {
        return new Promise(function(resolve, reject) {
        		return method(...args, function(err, result) {
                return err ? reject(err) : resolve(result);
            }.bind(this));//bind componenet scope to callback
        }.bind(this));//bind component scope to promise
    }//wrap async call inside a promise to await it
    async _oAuth(){
      let parse = await this._callbackToPromise(this._parseToken);
      const authorize = await this._callbackToPromise(this._authorize);
    }//wrap the async calls effect the post condition





    componentDidMount() {
      this._oAuth();

    }
    render() {
        return (
            <header className="bg-dark">
                <div className="container">
                    <nav className="navbar navbar-expand-lg pl-0 pr-0">
                        <a className="navbar-brand" href="/">
                            <h2 className="replica-bold pb-0 mb-0">EXIMCHAIN</h2>
                        </a>

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav11" aria-controls="navbarNav11" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNav11">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <a className="nav-link" href="/#solutions">Solutions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/team">Team</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/documents">Documents</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/road-map">Roadmap</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/faq">FAQ</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/token">Token</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="https://medium.com/eximchain" target="_blank" rel="noopener noreferrer">Blog</a>
                                </li>
                            </ul>
                            <button  className="btn btn-white btn-empty ml-lg-3" onClick={this._authenticate}>KYC Login</button>
                        </div>
                    </nav>
                </div>
            </header>
        );
    }
}
